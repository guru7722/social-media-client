import { type } from '@testing-library/user-event/dist/type'
import React from 'react'
import BTn from '../buttons/BTn'
import '../loginCard/LogInCard.css'
function LoginCard() {

    const formDATA = (e)=>{//handling form data on submit button pressed 
        e.preventDefault()
        console.log("form submited")
    }
  return (
    <div className='loginCard'>
        <form onSubmit={formDATA}>
        {/* Username */}
        <label htmlFor="username">
        <legend>username</legend>
        <input type="text" name='username' />
        </label>
        {/* password field */}
        <label htmlFor='password'>
        <legend>password</legend>
        <input type="password" name='password'/>
        </label>
        <BTn text={'LogIn'} type = {'submit'}/>
        </form>
        <BTn text={'SignIn with Google'}type = {'button'} />
        <a href='#'>Register</a><a href='#'>ForgetAccount</a>
    {/* <div className='LogInlinks'><a>ForgetAccount</a><a>RegisterAccount</a></div> */}
    </div>
  )
}

export default LoginCard