import React from 'react'
import '../buttons/Btn.css'
function BTn(props) {
  return (
    <><button className='btn' type={props.type}><p>{props.text}</p></button></>
  )
}

export default BTn